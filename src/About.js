import React, { Component } from 'react';

class About extends Component {
  render() {
    return (
      <main role="main">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h2>About</h2>
              <p>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                fermentum massa justo sit amet risus. Etiam porta sem malesuada
                magna mollis euismod. Donec sed odio dui.{' '}
              </p>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default About;
