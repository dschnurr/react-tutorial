import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './Home.js';
import About from './About.js';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    query: '',
    results: [],
  };
  onSearchChange = evt => {
    // "Fat arrow" syntax
    this.setState({ query: evt.target.value });
  };
  onSubmit = evt => {
    // Prevents the html form from submitting normally, we want to handle via ajax (fetch) instead
    evt.preventDefault();

    // Fetch results
    fetch('/search?q=' + this.state.query)
      .then(function(response) {
        return response.json();
      })
      .then(function(results) {
        this.setState({
          results: results,
        });
      });
  };
  render() {
    return (
      <Router>
        <div className="App">
          <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a className="navbar-brand" href="#">
              Navbar
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarsExampleDefault"
              aria-controls="navbarsExampleDefault"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarsExampleDefault"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <Link className="nav-link" to="/">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/about">
                    About
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link disabled" href="#">
                    Disabled
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="http://example.com"
                    id="dropdown01"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Dropdown
                  </a>
                  <div className="dropdown-menu" aria-labelledby="dropdown01">
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </li>
              </ul>
              <form
                onSubmit={this.onSubmit}
                className="form-inline my-2 my-lg-0"
              >
                <input
                  className="form-control mr-sm-2"
                  type="text"
                  placeholder="Search"
                  aria-label="Search"
                  value={this.state.query}
                  onChange={this.onSearchChange}
                />
                <button
                  className="btn btn-outline-success my-2 my-sm-0"
                  type="submit"
                >
                  Search
                </button>
                <div className="autocomplete-results">
                  {(this.state.results || []).map(result => {
                    return (
                      <div>
                        {result.name}
                      </div>
                    );
                  })}
                </div>
              </form>
            </div>
          </nav>

          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />

          <footer className="container">
            <p>&copy; Company 2017</p>
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
